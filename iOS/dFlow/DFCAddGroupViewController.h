//
//  DFCAddGroupViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "DFCUsersViewController.h"

@interface DFCAddGroupViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,  DFCUsersViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButtonItem;
@property (weak, nonatomic) IBOutlet UITextField *groupNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *membersTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *membersTableView;
@property (weak, nonatomic) IBOutlet UIButton *addMembersButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *uploadImageButton;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)backgroundPressed:(id)sender;
- (IBAction)addMembersButtonPressed:(id)sender;
- (IBAction)uploadImageButtonPressed:(id)sender;

@end
