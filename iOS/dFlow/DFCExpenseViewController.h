//
//  DFCExpenseViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

#import "TPKeyboardAvoidingScrollView.h"
#import "TSCurrencyTextField.h"
#import "JVFloatLabeledTextField.h"

@interface DFCExpenseViewController : UIViewController

@property (strong, nonatomic) PFObject *group;
@property (strong, nonatomic) TPKeyboardAvoidingScrollView *inputsScroller;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButtonItem;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *subjectTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *participantsTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *whoPaysTextfield;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dateTextField;
@property (weak, nonatomic) IBOutlet TSCurrencyTextField *quantityTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *addMembersButton;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;
- (IBAction)addUserButtonPressed:(id)sender;

@end
