//
//  DFCExpenseUsersViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 26/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol DFCExpenseUsersViewControllerDelegate

- (void)selectedUsers:(NSMutableArray *)selectedUsers;

@end

@interface DFCExpenseUsersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property id delegate;

@property (strong, nonatomic) PFObject *group;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UITableView *usersTableView;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)selectAllButtonPressed:(id)sender;

@end
