//
//  DFCViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 14/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCLoginViewController.h"

#import "ACSimpleKeychain.h"

@interface DFCLoginViewController ()

- (void)getFacebookData;

@end

@implementation DFCLoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
	
    // Do any additional setup after loading the view, typically from a nib.
    
    /*
    //Parse Test
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];
    */
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - public

- (void)getFacebookData {
    
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
        
        PFUser *currentUser = [PFUser currentUser];
        
        if (!error) {
            
            //TODO: Check if the data is available
            [currentUser setObject:[user objectForKey:@"id"] forKey:@"facebookId"];
            [currentUser setObject:[user objectForKey:@"name"] forKey:@"name"];
            
            if ([user objectForKey:@"location"][@"name"]) {
                
                [currentUser setObject:[user objectForKey:@"location"][@"name"] forKey:@"location"];
            }
            
            if ([user objectForKey:@"gender"]) {
                
                [currentUser setObject:[user objectForKey:@"gender"] forKey:@"gender"];
            }
            
            if ([user objectForKey:@"birthday"]) {
                
                [currentUser setObject:[user objectForKey:@"birthday"] forKey:@"birthday"];
            }
            
            if ([user objectForKey:@"relationship_status"]) {
                
                [currentUser setObject:[user objectForKey:@"relationship_status"] forKey:@"relationshipStatus"];
            }
            
            if ([user objectForKey:@"email"]) {
                
                [currentUser setObject:[user objectForKey:@"email"] forKey:@"email"];
            }
            
            [currentUser setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [user objectForKey:@"id"]] forKey:@"profilePicture"];
            
            [currentUser saveInBackground];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            
            NSLog(@"Error getting email...");
        }
    }];
}

#pragma mark - Action

- (IBAction)facebookButtonPressed:(id)sender {
    
    //ACSimpleKeychain *keychain = [ACSimpleKeychain defaultKeychain];
    
    NSArray *permissions = @[@"public_profile", @"email", @"user_friends", @"user_photos"];
    
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *facebookUser, NSError *error) {
        
        if (!facebookUser) {
            
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (facebookUser.isNew) {
            
            NSLog(@"User signed up and logged in through Facebook!");
            [self getFacebookData];
            
        } else {
            
            NSLog(@"User logged in through Facebook!");
            
            //TODO: Request email only if it's not in our database
            [self getFacebookData];
        }
    }];
}

@end
