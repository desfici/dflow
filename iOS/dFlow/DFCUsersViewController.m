//
//  DFCUsersViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCUsersViewController.h"

#import "DFCUserTableViewCell.h"

#import "DFCContactsUtils.h"

@interface DFCUsersViewController ()

@property (strong, nonatomic) NSArray *facebookContacts;
@property (strong, nonatomic) NSArray *addressbookContacts;
@property (strong, nonatomic) NSMutableArray *selectedContacts;

@end

@implementation DFCUsersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"AddUsers", nil);
    
    self.facebookContacts = [[NSArray alloc] init];
    self.selectedContacts = [[NSMutableArray alloc] init];
    
    [self.closeButton setTitle:NSLocalizedString(@"Close", nil)];
    [self.addButton setTitle:NSLocalizedString(@"Add", nil)];
    
    //Issue a Facebook Graph API request to get your user's friend list
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (!error) {
            
            //Result will contain an array with your user's friends in the "data" key
            NSArray *friendObjects = [result objectForKey:@"data"];
            NSMutableArray *friendIds = [NSMutableArray arrayWithCapacity:friendObjects.count];
            
            //Create a list of friends' Facebook IDs
            for (NSDictionary *friendObject in friendObjects) {
                
                [friendIds addObject:[friendObject objectForKey:@"id"]];
            }
            
            //Construct a PFUser query that will find friends whose facebook ids are contained in the current user's friend list.
            PFQuery *friendQuery = [PFUser query];
            [friendQuery whereKey:@"facebookId" containedIn:friendIds];
            
            //FindObjects will return a list of PFUsers that are friends with the current user
            self.facebookContacts = [[friendQuery findObjects] copy];
            
            [self.usersTableView reloadData];
        }
    }];
    
    self.addressbookContacts = [DFCContactsUtils getAllContacts];
    
    /*
    [FBRequestConnection startWithGraphPath:@"/me/taggable_friends" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        for (NSDictionary *facebookUser in [result objectForKey:@"data"]) {
            
            PFUser *user = [[PFUser alloc] init];
            
            [user setObject:[facebookUser objectForKey:@"name"] forKey:@"name"];
            [user setObject:[facebookUser objectForKey:@"id"] forKey:@"id"];
            [user setObject:[[[facebookUser objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"] forKey:@"profilePictureThumb"];
            
            [self.users addObject:user];
        }
        
        [self.usersTableView reloadData];
    }];
    */
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    int numberOfRows = 0;
    
    switch (section) {
            
        case 0:
            
            numberOfRows = [self.facebookContacts count];
            
            break;
            
        case 1:
            
            numberOfRows = [self.addressbookContacts count];
            
            break;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DFCUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    
    PFUser *facebookUser;
    DFCContact *contactsUser;

    switch (indexPath.section) {
            
        case 0:
            
            facebookUser = [self.facebookContacts objectAtIndex:indexPath.row];
            
            [cell.userNameLabel setText:[facebookUser objectForKey:@"name"]];
            [cell.userImage sd_setImageWithURL:[facebookUser objectForKey:@"profilePicture"]placeholderImage:[UIImage new]];
            
            break;
            
        case 1:
            
            contactsUser = [self.addressbookContacts objectAtIndex:indexPath.row];
            
            [cell.userNameLabel setText:contactsUser.completeName];
            
            break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *sectionName = @"";
    
    switch (section) {
            
        case 0:
            
            sectionName = NSLocalizedString(@"FriendsDflow", nil);
            
            break;
            
        case 1:
            
            sectionName = NSLocalizedString(@"Contacts", nil);

            
            break;
    }
    
    return sectionName;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
            
        case 0:
            
            [self.selectedContacts addObject:[self.facebookContacts objectAtIndex:indexPath.row]];
            
            break;
            
        case 1:
            
            [self.selectedContacts addObject:[self.addressbookContacts objectAtIndex:indexPath.row]];
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
            
        case 0:
            
            [self.selectedContacts removeObject:[self.facebookContacts objectAtIndex:indexPath.row]];
            
            break;
            
        case 1:
            
            [self.selectedContacts removeObject:[self.addressbookContacts objectAtIndex:indexPath.row]];
            
            break;
    }
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addButtonPressed:(id)sender {
    
    [self.delegate selectedUsers:self.selectedContacts];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
