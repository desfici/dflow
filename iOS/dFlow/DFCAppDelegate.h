//
//  DFCAppDelegate.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 14/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface DFCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
