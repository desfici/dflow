//
//  DFCGroupViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface DFCGroupViewController : UIViewController

@property (strong, nonatomic) PFObject *group;
@property (weak, nonatomic) IBOutlet UILabel *membersLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfMembersLabel;
@property (weak, nonatomic) IBOutlet UILabel *iOweLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountIOweLabel;
@property (weak, nonatomic) IBOutlet UIButton *settleButton;
@property (weak, nonatomic) IBOutlet UITableView *expensesTableView;
@property (weak, nonatomic) IBOutlet UIImageView *groupImage;
@property (weak, nonatomic) IBOutlet UIView *summaryView;
@property (weak, nonatomic) IBOutlet UIView *participantsAmountSeparatorView;
@property (weak, nonatomic) IBOutlet UIView *amountSettleSeparatorView;

- (IBAction)settleButtonPressed:(id)sender;
- (IBAction)addExpenseButtonPressed:(id)sender;

@end
