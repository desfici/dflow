//
//  DFCGroupsViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 18/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCGroupsViewController.h"

#import "ACSimpleKeychain.h"

#import "DFCGroupTableViewCell.h"

#import "DFCColors.h"

#import "DFCGroupViewController.h"

@interface DFCGroupsViewController ()

@property (strong, nonatomic) NSArray *groups;

- (BOOL)isLogged;
- (void)refreshGroups;

@end

@implementation DFCGroupsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //TODO: Check if user still valid doing a me request, otherwise logout
    if (![self isLogged]) {
        
        // Push the next view controller without animation
        [self performSegueWithIdentifier:@"groupsToLogin" sender:nil];
    }
    
    //Colors
    [self.summaryView setBackgroundColor:[DFCColors lightGrayColor]];
    [self.separatorView setBackgroundColor:[DFCColors mediumGrayColor]];
    [self.oweMeLabel setTextColor:[DFCColors mediumGrayColor]];
    [self.iOweLabel setTextColor:[DFCColors mediumGrayColor]];
    [self.oweMeAmountLabel setTextColor:[DFCColors blueColor]];
    [self.iOweAmountLabel setTextColor:[DFCColors redColor]];
    
    //Localization
    self.title = NSLocalizedString(@"Groups", nil);
    
    [self.oweMeLabel setText:NSLocalizedString(@"OweMe", nil)];
    [self.iOweLabel setText:NSLocalizedString(@"IOwe", nil)];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self refreshGroups];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"groupsToGroup"]) {
        
        DFCGroupViewController *groupViewController = segue.destinationViewController;
        groupViewController.group = sender;
    }
}

#pragma mark - public

- (BOOL)isLogged {
    
    return ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]);
}

- (void)refreshGroups {
    
    if ([self isLogged]) {
        
        PFQuery *query = [PFQuery queryWithClassName:@"Group"];
        [query whereKey:@"users" equalTo:[PFUser currentUser]];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
            
            if (!error) {
                
                self.groups = [NSArray arrayWithArray:objects];
                
                [self.groupsTableView reloadData];
            }
            else {
                
                NSLog(@"Error retreiving groups...");
            }
        }];
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.groups count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DFCGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCell" forIndexPath:indexPath];
    
    PFObject *group = [self.groups objectAtIndex:indexPath.row];
    
    [cell fillGroupCellWithGroup:group];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"groupsToGroup" sender:[self.groups objectAtIndex:indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

#pragma mark - Action

- (IBAction)logoutButtonPressed:(id)sender {
    
    [PFUser logOut];
    
    [self performSegueWithIdentifier:@"groupsToLogin" sender:nil];
}

@end
