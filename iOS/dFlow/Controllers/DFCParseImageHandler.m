//
//  DFCParseImageHandler.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCParseImageHandler.h"

#import "TNSexyImageUploadProgress.h"

@interface DFCParseImageHandler()

@property (strong, nonatomic) TNSexyImageUploadProgress *imageUploadProgress;

@end

@implementation DFCParseImageHandler

#pragma mark - public

+ (void)uploadPictureWith:(NSData *)data name:(NSString *)name andClassName:(NSString *)className withCompletition:(void (^)(BOOL succeeded, PFObject *groupPhoto))completition {
    
    PFFile *imageFile = [PFFile fileWithName:name data:data];
    __block PFObject *groupPhoto;
    
    TNSexyImageUploadProgress *imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
    imageUploadProgress.radius = 100;
    imageUploadProgress.progressBorderThickness = -10;
    imageUploadProgress.trackColor = [UIColor blackColor];
    imageUploadProgress.progressColor = [UIColor whiteColor];
    
    imageUploadProgress.imageToUpload = [UIImage imageWithData:data];
    [imageUploadProgress show];
    
    // Save PFFile
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            // Create a PFObject around a PFFile and associate it with the current user
            groupPhoto = [PFObject objectWithClassName:className];
            [groupPhoto setObject:imageFile forKey:@"imageFile"];
            
            [groupPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    completition(succeeded, groupPhoto);
                });
            }];
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                completition(NO, nil);
            });
        }
    } progressBlock:^(int percentDone) {
        
        NSLog(@"Percent: %i", percentDone);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            imageUploadProgress.progress = percentDone / 100.0;
        });
    }];
}

@end
