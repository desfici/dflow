//
//  DFCParseGroupHandler.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCParseGroupHandler.h"

@implementation DFCParseGroupHandler

+ (void)createGroupWithName:(NSString *)name image:(PFObject *)image andMembers:(NSArray *)members withCompletition:(void(^)(BOOL succeeded))completition {
    
    PFObject *group = [PFObject objectWithClassName:@"Group"];
    
    //Name
    group[@"name"] = name;
    
    //Image
    group[@"image"] = image;
    
    //Users (users = dFlow users, guestUsers = users without dFlow) and initialization of its debts to 0
    PFRelation *relationUsers = [group relationForKey:@"users"];
    PFRelation *relationGuestUsers = [group relationForKey:@"guestUsers"];
    
    NSMutableArray *usersDebts = [[NSMutableArray alloc] init];
    
    for (id user in members) {
        
        if ([user isKindOfClass:[PFUser class]]) {
            
            [relationUsers addObject:user];
            [group.ACL setWriteAccess:YES forUser:user];
        }
        else {
            
            [relationGuestUsers addObject:user];
        }
        
        NSMutableDictionary *userWithDebt = [NSMutableDictionary dictionaryWithObjectsAndKeys:user, @"user", @0, @"debt", nil];
        [usersDebts addObject:userWithDebt];
    }
    
    [group setObject:usersDebts forKey:@"usersDebts"];
    
    //Number of participants
    [group setObject:[NSNumber numberWithInt:[members count]] forKey:@"numberOfParticipants"];
    
    [group saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        
        completition(succeeded);
    }];
}

@end
