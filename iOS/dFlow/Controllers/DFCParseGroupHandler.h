//
//  DFCParseGroupHandler.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface DFCParseGroupHandler : NSObject

+ (void)createGroupWithName:(NSString *)name image:(PFObject *)image andMembers:(NSArray *)members withCompletition:(void(^)(BOOL succeeded))completition;

@end
