//
//  DFCImagePickerHandler.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 25/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCImagePickerHandler.h"

#import "DZNPhotoPickerController.h"
#import "UIImagePickerControllerExtended.h"

@interface DFCImagePickerHandler () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIActionSheet *actionSheet;

- (void)presentImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType;
- (void)handleImagePicker:(UIImagePickerController *)picker withMediaInfo:(NSDictionary *)info;
- (void)presentController:(UIViewController *)controller ;
- (void)dismissController:(UIViewController *)controller;

@end

@implementation DFCImagePickerHandler

- (instancetype)initWithViewController:(UIViewController <DFCImagePickerHandlerDelegate> *)viewController {
    
    self = [super init];
    
    if (self) {
        
        self.viewController = viewController;
        self.delegate = viewController;
    }
    
    return self;
}

#pragma mark - public

- (void)showOptions {
    
    if (!self.actionSheet) {
        
        self.actionSheet = [UIActionSheet new];
    
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        
        [self.actionSheet addButtonWithTitle:NSLocalizedString(@"Take Photo", nil)];
    }
    
    [self.actionSheet addButtonWithTitle:NSLocalizedString(@"Choose Photo", nil)];
    //[self.actionSheet addButtonWithTitle:NSLocalizedString(@"Search Photo", nil)];
    
    /*
    if (self.imageView.image) {
        
        [self.actionSheet addButtonWithTitle:NSLocalizedString(@"Edit Photo", nil)];
        [self.actionSheet addButtonWithTitle:NSLocalizedString(@"Delete Photo", nil)];
    }
    */
    
    [self.actionSheet setCancelButtonIndex:[self.actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)]];
    [self.actionSheet setDelegate:self];
    
    }
        
    CGRect rect = self.viewController.view.frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        rect.origin = CGPointMake(rect.origin.x, rect.origin.y + rect.size.height/2);
    }
    
    [self.actionSheet showFromRect:rect inView:self.viewController.view animated:YES];
}

#pragma mark - private

- (void)presentImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    picker.cropMode = DZNPhotoEditorViewControllerCropModeNone;
    
    picker.finalizationBlock = ^(UIImagePickerController *picker, NSDictionary *info) {
        [self handleImagePicker:picker withMediaInfo:info];
    };
    
    picker.cancellationBlock = ^(UIImagePickerController *picker) {
        
        [self dismissController:picker];
    };
    
    [self presentController:picker];
}

- (void)handleImagePicker:(UIImagePickerController *)picker withMediaInfo:(NSDictionary *)info {
    
    if (picker.cropMode != DZNPhotoEditorViewControllerCropModeNone) {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        DZNPhotoEditorViewController *editor = [[DZNPhotoEditorViewController alloc] initWithImage:image cropMode:picker.cropMode];
        [picker pushViewController:editor animated:YES];
    }
    else {
        
        //[self updateImageWithPayload:info];
        
        [self.delegate imageSelected:[info objectForKey:UIImagePickerControllerOriginalImage]];
        
        [self dismissController:picker];
    }
}

- (void)presentController:(UIViewController *)controller {
    
    /*
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        _popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
        _popoverController.popoverContentSize = CGSizeMake(320.0, 548.0);
        
        [_popoverController presentPopoverFromRect:_button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
    */
        [self.viewController presentViewController:controller animated:YES completion:NULL];
    //}
}

- (void)dismissController:(UIViewController *)controller {
    
    /*
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
     
        [_popoverController dismissPopoverAnimated:YES];
    }
    else {
    */
        [controller dismissViewControllerAnimated:YES completion:NULL];
    //}
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Take Photo", nil)]) {
        
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Choose Photo", nil)]) {
        
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    /*
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Search Photo",nil)]) {
        [self presentPhotoPicker];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Edit Photo",nil)]) {
        [self presentPhotoEditor];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Delete Photo",nil)]) {
        [self startupConfig];
    }
    */
}


@end
