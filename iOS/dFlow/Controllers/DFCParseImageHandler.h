//
//  DFCParseImageHandler.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface DFCParseImageHandler : NSObject

+ (void)uploadPictureWith:(NSData *)data name:(NSString *)name andClassName:(NSString *)className withCompletition:(void (^)(BOOL succeeded, PFObject *groupPhoto))completition;

@end
