//
//  DFCContactsUtils.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface DFCContact : NSObject

@property (strong, nonatomic) NSString *completeName;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *email;

@end

@interface DFCContactsUtils : NSObject

+ (NSArray *)getAllContacts;

@end
