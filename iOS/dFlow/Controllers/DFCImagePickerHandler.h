//
//  DFCImagePickerHandler.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 25/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DFCImagePickerHandlerDelegate

- (void)imageSelected:(UIImage *)imageSelected;

@end

@interface DFCImagePickerHandler : NSObject

@property id delegate;

- (instancetype)initWithViewController:(UIViewController <DFCImagePickerHandlerDelegate> *)viewController;
- (void)showOptions;

@end
