//
//  DFCParseUserHandler.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface DFCParseUserHandler : NSObject

+ (NSArray *)saveUsers:(NSArray *)users;

@end
