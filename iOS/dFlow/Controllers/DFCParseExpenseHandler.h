//
//  DFCParseExpenseHandler.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 22/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFCParseExpenseHandler : NSObject

+ (void)createExpenseWithSubject:(NSString *)subject;

@end
