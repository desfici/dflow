//
//  DFCImagesUtils.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 18/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFCImagesUtils : NSObject

+ (void)roundedImage:(UIImageView *)imageView;

@end
