//
//  DFCContactsUtils.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCContactsUtils.h"

@implementation DFCContact

@end

@implementation DFCContactsUtils

+ (NSArray *)getAllContacts{
    
    CFErrorRef *error = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else { // we're on iOS 5 or older
        
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
#ifdef DEBUG
        NSLog(@"Fetching contact info ----> ");
#endif
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        NSMutableArray* items = [NSMutableArray arrayWithCapacity:nPeople];
        
        for (int i = 0; nPeople > 0 && i < nPeople; i++) {
            
            DFCContact *contact = [[DFCContact alloc] init];
            
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //Get First Name and Last Name
            NSString *completeName = @"";
            
            NSString *name = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            NSString *surname = (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            if (name) {
                
                completeName = [completeName stringByAppendingString:[NSString stringWithFormat:@"%@ ", name]];
            }
            
            if (surname) {
                
                completeName = [completeName stringByAppendingString:surname];
            }
            
            [contact setCompleteName:completeName];
            
            /*
            //Fetch contacts picture, if pic doesn't exists, show standart one
            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
            
            contacts.image = [UIImage imageWithData:imgData];
            
            if (!contacts.image) {
                
                contacts.image = [UIImage imageNamed:@"NOIMG.png"];
            }
            */
            
            //Get Phone Numbers
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            
            for(CFIndex i = 0; i < ABMultiValueGetCount(multiPhones); i++) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [phoneNumbers addObject:phoneNumber];
                
                //NSLog(@"All numbers %@", phoneNumbers);
            }
            
            //TODO: Get all
            if ([phoneNumbers count] > 0) {
                
                [contact setPhoneNumber:[phoneNumbers firstObject]];
            }
            
            //Get Contact email
            NSMutableArray *contactEmails = [NSMutableArray new];
            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
                
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
                
                [contactEmails addObject:contactEmail];
                // NSLog(@"All emails are:%@", contactEmails);
            }
            
            //TODO: Get all
            if ([contactEmails count] > 0) {
                
                [contact setEmail:[contactEmails firstObject]];
            }
            
            if (![completeName isEqualToString:@""]) {
                
                [items addObject:contact];
            }
            
#ifdef DEBUG
            //NSLog(@"Person is: %@", contacts.firstNames);
            //NSLog(@"Phones are: %@", contacts.numbers);
            //NSLog(@"Email is:%@", contacts.emails);
#endif
            
        }
        
        return items;
        
    } else {
        
#ifdef DEBUG
        NSLog(@"Cannot fetch Contacts :( ");
#endif
        return NO;
    }
}

@end
