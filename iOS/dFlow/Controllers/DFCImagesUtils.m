//
//  DFCImagesUtils.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 18/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCImagesUtils.h"

@implementation DFCImagesUtils

+ (void)roundedImage:(UIImageView *)imageView {
    
    imageView.layer.cornerRadius = imageView.frame.size.width / 2.0;
}

@end
