//
//  DFCParseUserHandler.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCParseUserHandler.h"

#import "DFCContactsUtils.h"

@implementation DFCParseUserHandler

+ (NSArray *)saveUsers:(NSArray *)users {
    
    NSMutableArray *members = [[NSMutableArray alloc] init];
    
    for (id user in users) {
        
        if ([user isKindOfClass:[PFUser class]]) {
            
            [members addObject:user];
        }
        else {
            
            DFCContact *contactUser = (DFCContact *)user;
            
            PFObject *guestUser = [PFObject objectWithClassName:@"GuestUser"];
            
            if (contactUser.completeName) {
                
                [guestUser setObject:contactUser.completeName forKey:@"completeName"];
            }
            
            if (contactUser.email) {
                
                [guestUser setObject:contactUser.email forKey:@"email"];
            }
            
            if (contactUser.phoneNumber) {
                
                [guestUser setObject:contactUser.phoneNumber forKey:@"phoneNumber"];
            }
            
            [members addObject:guestUser];
        }
    }
    
    return members;
}

@end
