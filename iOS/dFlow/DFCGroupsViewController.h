//
//  DFCGroupsViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 18/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface DFCGroupsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *groupsTableView;
@property (weak, nonatomic) IBOutlet UILabel *oweMeLabel;
@property (weak, nonatomic) IBOutlet UILabel *oweMeAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *iOweLabel;
@property (weak, nonatomic) IBOutlet UILabel *iOweAmountLabel;
@property (weak, nonatomic) IBOutlet UIView *oweMeView;
@property (weak, nonatomic) IBOutlet UIView *iOweView;
@property (weak, nonatomic) IBOutlet UIView *summaryView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

- (IBAction)logoutButtonPressed:(id)sender;

@end
