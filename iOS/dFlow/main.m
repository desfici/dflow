//
//  main.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 14/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DFCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DFCAppDelegate class]));
    }
}
