//
//  DFCExpenseViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCExpenseViewController.h"

#import "DFCExpenseUsersViewController.h"

#import "DFCColors.h"

#import "DFCParseExpenseHandler.h"

const static CGFloat kJVFieldHeight = 44.0f;

@interface DFCExpenseViewController () <DFCExpenseUsersViewControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSString *subject;
@property (strong, nonatomic) NSNumber *amount;
@property (strong, nonatomic) NSArray *participantUsers;
@property (strong, nonatomic) NSDate *date;

@property (strong, nonatomic) PFUser *payer;

- (void)customizeLabel:(JVFloatLabeledTextField *)textField;
- (void)dismissKeyboard;
- (NSString *)surveyHasInvalidData;

@end

@implementation DFCExpenseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.payer = [PFUser currentUser];
    
    [self.whoPaysTextfield setText:[[PFUser currentUser] objectForKey:@"name"]];
    
    //Date
    self.date = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"dd/MM/yy"];
    
    [self.dateTextField setText:[dateFormat stringFromDate:self.date]];
    
    //Textfields customization
    [self customizeLabel:self.subjectTextField];
    [self customizeLabel:self.participantsTextField];
    [self customizeLabel:self.whoPaysTextfield];
    [self customizeLabel:self.dateTextField];
    
    CGRect subjectTextFieldFrame = self.quantityTextField.frame;
    subjectTextFieldFrame.size.height = kJVFieldHeight;
    
    [self.quantityTextField setFrame:subjectTextFieldFrame];
    
    //Add members button
    CGPoint addMembersButtonCenter = self.addMembersButton.center;
    addMembersButtonCenter.y = self.participantsTextField.center.y;
    
    [self.addMembersButton setCenter:addMembersButtonCenter];
    
    //Localization
    self.title = NSLocalizedString(@"AddExpense", nil);
    
    [self.closeButtonItem setTitle:NSLocalizedString(@"Close", nil)];
    [self.addButtonItem setTitle:NSLocalizedString(@"Add", nil)];
    
    [self.subjectTextField setPlaceholder:NSLocalizedString(@"Subject", nil)];
    [self.whoPaysTextfield setPlaceholder:NSLocalizedString(@"WhoPays", nil)];
    [self.participantsTextField setPlaceholder:NSLocalizedString(@"DivideExpensesBetween", nil)];
    [self.dateTextField setPlaceholder:NSLocalizedString(@"Date", nil)];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)customizeLabel:(JVFloatLabeledTextField *)textField {
    
    CGRect subjectTextFieldFrame = textField.frame;
    subjectTextFieldFrame.size.height = kJVFieldHeight;
    
    [textField setFrame:subjectTextFieldFrame];
    [textField setBorderStyle:UITextBorderStyleNone];
    textField.floatingLabelTextColor = [DFCColors blueColor];
}

- (void)dismissKeyboard {
    
    [self.view endEditing:YES];
}

- (NSString *)surveyHasInvalidData {
    
    NSString *message;
    
    if (!self.subjectTextField.text ||[self.subjectTextField.text isEqualToString:@""]) {
        
        message = NSLocalizedString(@"SubjectNameError", nil);
    }
    else if ([self.amount floatValue] == 0.0) {
        
        message = NSLocalizedString(@"AddAtLeastOneMemberError", nil);
    }
    else if (!self.participantsTextField.text ||[self.participantsTextField.text isEqualToString:@""]) {
        
        message = NSLocalizedString(@"ParticipantsError", nil);
    }
    
    return message;
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addButtonPressed:(id)sender {

    PFObject *expense = [[PFObject alloc] initWithClassName:@"Expense"];
    
    NSString *errorMessage = [self surveyHasInvalidData];
    
    if (!errorMessage) {
        
        //[DFCParseExpenseHandler createExpenseWithSubject:self.subject amount:self.amount payer:self.payer usersAndWeights:usersAndWeights];
    }
    else {
        
    }
    
    //Add subject
    self.subject = self.subjectTextField.text;
    
    [expense setObject:self.subject forKey:@"subject"];
    
    //Format and add amount
    NSMutableString *amountText = [self.quantityTextField.text mutableCopy];
    NSString *currencySymbol = self.quantityTextField.currencyNumberFormatter.currencySymbol;
    
    [amountText replaceOccurrencesOfString:currencySymbol withString:@"" options:NSBackwardsSearch range:NSMakeRange(0, [amountText length])];
    
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    self.amount = [formatter numberFromString:amountText];
    
    [expense setObject:self.amount forKey:@"amount"];
    
    //Add payer
    [expense setObject:self.payer forKey:@"payer"];
    
    //Add users
    NSMutableArray *usersAndWeights = [[NSMutableArray alloc] init];
    
    for (id user in self.participantUsers) {
        
        NSDictionary *userWithWeight = [NSDictionary dictionaryWithObjectsAndKeys:user, @"user", @1, @"weight", nil];
            
        [usersAndWeights addObject:userWithWeight];
    }
    
    [expense addObject:usersAndWeights forKey:@"usersAndWeights"];
    
    //Add date
    [expense setObject:self.date forKey:@"date"];
    
    [expense saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        
        if (error) {
            
            NSLog(@"Error saving expense");
        }
        else {
            
            PFQuery *query = [PFQuery queryWithClassName:@"Group"];
            [query whereKey:@"objectId" equalTo:self.group.objectId];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
                
                if (error) {
                    
                    NSLog(@"Error getting group");
                }
                else {
               
                    PFObject *group = [objects firstObject];
                    
                    //Add the expense to the group
                    PFRelation *relation = [group relationForKey:@"expenses"];
                    
                    [relation addObject:expense];
                    
                    //Update users's debt in the group
                    NSMutableArray *usersDebts = [group objectForKey:@"usersDebts"];
                    
                    if (!usersDebts) {
                        
                        usersDebts = [[NSMutableArray alloc] init];
                    }
                    
                    for (id user in self.participantUsers) {
                        
                        float debt;
                        
                        if ([[user objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                            
                            debt = ([self.amount floatValue] / [self.participantUsers count]) * ([self.participantUsers count] - 1);
                        }
                        else {
                            
                            debt = - ([self.amount floatValue] / [self.participantUsers count]);
                        }
                        
                        //Update users debt if existed
                        NSMutableArray *usersDebtsCopy = [NSMutableArray arrayWithArray:usersDebts];
                        
                        for (NSDictionary *userDebt in usersDebts) {
                            
                            PFUser *currentUser = [userDebt objectForKey:@"user"];
                            
                            if ([[currentUser objectId] isEqualToString:[user objectId]]) {
                                
                                NSMutableDictionary *userWithDebt = [NSMutableDictionary dictionaryWithObjectsAndKeys:currentUser, @"user", [NSNumber numberWithFloat:debt], @"debt", nil];
                                
                                debt += [[userDebt objectForKey:@"debt"] floatValue];
                                
                                [usersDebtsCopy removeObject:userDebt];
                                
                                [userWithDebt setObject:[NSNumber numberWithFloat:debt] forKey:@"debt"];
                                
                                [usersDebtsCopy addObject:userWithDebt];
                            }
                        }
                        
                        [usersDebts removeAllObjects];
                        [usersDebts addObjectsFromArray:usersDebtsCopy];
                    }
                    
                    [group setObject:usersDebts forKey:@"usersDebts"];
                    
                    [group saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                        
                        if (error) {
                            
                            NSLog(@"Error adding a new amount");
                        }
                        else {
                            
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }
                    }];
                }
            }];
        }
    }];
}

- (IBAction)addUserButtonPressed:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"AddMembers"];
    
    PFObject *group = self.group;
    
    DFCExpenseUsersViewController *expenseUsersViewController = [[(UINavigationController *)navigationController viewControllers] firstObject];
    expenseUsersViewController.title = NSLocalizedString(@"DivideExpenses", nil);
    expenseUsersViewController.delegate = self;
    expenseUsersViewController.group = group;
    
    [self presentViewController:navigationController animated:YES completion:^{}];
}

#pragma mark - DFCExpenseUsersViewControllerDelegate

- (void)selectedUsers:(NSMutableArray *)selectedUsers {
    
    NSString *selectedUsersNames = @"";
    
    for (id selectedUser in selectedUsers) {
        
        if ([selectedUser isKindOfClass:[PFUser class]]) {
            
            selectedUsersNames = [selectedUsersNames stringByAppendingString:[NSString stringWithFormat:@"%@, ", [selectedUser objectForKey:@"name"]]];
        }
        else {
            
            selectedUsersNames = [selectedUsersNames stringByAppendingString:[NSString stringWithFormat:@"%@, ", [selectedUser objectForKey:@"completeName"]]];
        }
    }
    
    //Remove last comma and space characters
    selectedUsersNames = [selectedUsersNames substringToIndex:[selectedUsersNames length] - 2];
    
    [self.participantsTextField setText:selectedUsersNames];
    
    self.participantUsers = [NSArray arrayWithArray:selectedUsers];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    
    //[textView adjustHeightIfNeeded:0.0f];
}

@end
