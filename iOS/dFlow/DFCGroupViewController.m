//
//  DFCGroupViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 21/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCGroupViewController.h"

#import "DFCExpenseViewController.h"

#import "DFCColors.h"

#import "DFCExpenseTableViewCell.h"

@interface DFCGroupViewController ()

@property (strong, nonatomic) NSArray *expenses;

@end

@implementation DFCGroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.title = [self.group objectForKey:@"name"];
    
    //Get number of participants
    [self.numberOfMembersLabel setText:[NSString stringWithFormat:@"%@", [self.group objectForKey:@"numberOfParticipants"]]];
    
    //Get group image
    PFObject *groupImage = [self.group objectForKey:@"image"];
    
    PFQuery *imageQuery = [PFQuery queryWithClassName:@"GroupPhoto"];
    [imageQuery whereKey:@"objectId" equalTo:groupImage.objectId];
    
    [imageQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        
        PFFile *imageFile = [[objects firstObject] objectForKey:@"imageFile"];
        
        [self.groupImage sd_setImageWithURL:[NSURL URLWithString:[imageFile url]]];
    }];
    
    //Debt
    NSArray *usersDebts = [self.group objectForKey:@"usersDebts"];
    
    for (NSDictionary *userDebt in usersDebts) {
        
        PFUser *user = [userDebt objectForKey:@"user"];
        
        if ([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            
            float debt = [[userDebt objectForKey:@"debt"] floatValue];
            
            [self.amountIOweLabel setText:[NSString stringWithFormat:@"%.2f €", debt]];
            
            if (debt > 0) {
                
                [self.iOweLabel setText:NSLocalizedString(@"OweMe", nil)];
                [self.amountIOweLabel setTextColor:[DFCColors blueColor]];
            }
            else {
                
                [self.iOweLabel setText:NSLocalizedString(@"IOwe", nil)];
                [self.amountIOweLabel setTextColor:[DFCColors redColor]];
            }
        }
    }
    
    //Colors
    [self.summaryView setBackgroundColor:[DFCColors lightGrayColor]];
    [self.participantsAmountSeparatorView setBackgroundColor:[DFCColors mediumGrayColor]];
    [self.amountSettleSeparatorView setBackgroundColor:[DFCColors mediumGrayColor]];
    [self.membersLabel setTextColor:[UIColor blackColor]];
    [self.iOweLabel setTextColor:[UIColor blackColor]];
    [self.numberOfMembersLabel setTextColor:[DFCColors mediumGrayColor]];
    [self.settleButton setBackgroundColor:[DFCColors blueColor]];
    
    //Localization
    [self.membersLabel setText:[NSLocalizedString(@"Members", nil) lowercaseString]];
    [self.settleButton setTitle:NSLocalizedString(@"Settle", nil) forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    PFRelation *expenses = [self.group relationForKey:@"expenses"];
    
    [[expenses query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
    
        self.expenses = [objects copy];
        
        [self.expensesTableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.expenses count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DFCExpenseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExpenseCell" forIndexPath:indexPath];
    
    PFObject *expense = [self.expenses objectAtIndex:indexPath.row];
    
    [cell fillCellWithExpense:expense];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

#pragma mark - Action

- (IBAction)settleButtonPressed:(id)sender {

}

- (IBAction)addExpenseButtonPressed:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AddExpense" bundle:nil];
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"AddExpense"];
    
    DFCExpenseViewController *expensesViewController = [[(UINavigationController *)navigationController viewControllers] firstObject];
    expensesViewController.group = self.group;
    
    [self presentViewController:navigationController animated:YES completion:^{}];
}

@end
