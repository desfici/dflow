//
//  DFCAddGroupViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCAddGroupViewController.h"

#import "CSNotificationView.h"

#import "DFCColors.h"
#import "DFCGuestUser.h"

#import "DFCUserTableViewCell.h"

#import "DFCImagePickerHandler.h"
#import "DFCContactsUtils.h"
#import "DFCParseImageHandler.h"
#import "DFCParseGroupHandler.h"
#import "DFCParseUserHandler.h"

@interface DFCAddGroupViewController () <DFCImagePickerHandlerDelegate>

@property (strong, nonatomic) NSMutableArray *members;

@property (strong, nonatomic) PFObject *groupPhoto;
@property (strong, nonatomic) DFCImagePickerHandler *imagePickerHandler;

- (void)uploadImage:(NSData *)data;
- (NSString *)surveyHasInvalidData;

@end

@implementation DFCAddGroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(@"CreateGroup", nil);
    
    //Initializations
    self.members = [NSMutableArray arrayWithObject:[PFUser currentUser]];
    
    self.imagePickerHandler = [[DFCImagePickerHandler alloc] initWithViewController:self];
    
    //Localization
    [self.membersTitleLabel setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(@"Members", nil)]];
    [self.closeButtonItem setTitle:NSLocalizedString(@"Close", nil)];
    [self.groupNameTextField setPlaceholder:NSLocalizedString(@"GroupName", nil)];
    [self.saveButtonItem setTitle:NSLocalizedString(@"Save", nil)];
    [self.addMembersButton setTitle:NSLocalizedString(@"AddMembers", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"addgroupToFriends"]) {
        
        DFCUsersViewController *usersViewController = [[(UINavigationController *)segue.destinationViewController viewControllers] firstObject];
        usersViewController.delegate = self;
    }
}

#pragma mark - private

- (void)uploadImage:(NSData *)imageData {
    
    //TODO: Remove image from ddbb if group not created
    NSString *fileName = @"Group.jpg";
    
    //TODO: Set the group's name to the image (without special characters)
    if (![self.groupNameTextField.text isEqualToString:@""]) {
        
        //fileName = [NSString stringWithFormat:@"%@.jpg", self.groupNameTextField.text];
    }
    
    [DFCParseImageHandler uploadPictureWith:imageData name:fileName andClassName:@"GroupPhoto" withCompletition:^(BOOL succeeded, PFObject *groupPhoto){
    
        if (succeeded) {
            
            self.groupPhoto = groupPhoto;
        }
        else {
           
            NSString *errorMessage = NSLocalizedString(@"UnknownError", nil);
            
            [CSNotificationView showInViewController:self
                                               style:CSNotificationViewStyleError
                                             message:errorMessage
                                    withCompletition:^{}];
        }
    }];
}

- (NSString *)surveyHasInvalidData {
    
    NSString *message;

    if (!self.groupNameTextField.text ||[self.groupNameTextField.text isEqualToString:@""]) {
        
        message = NSLocalizedString(@"AddANameError", nil);
    }
    else if ([self.members count] < 2) {
        
        message = NSLocalizedString(@"AddAtLeastOneMemberError", nil);
    }
    else if (!self.groupPhoto) {
        
        message = NSLocalizedString(@"AddAPictureError", nil);
    }
    
    return message;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.members count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DFCUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    
    id member = [self.members objectAtIndex:indexPath.row];
    
    if ([member isKindOfClass:[PFUser class]]) {
        
        PFUser *user = member;
        
        [cell.userNameLabel setText:[user objectForKey:@"name"]];
        [cell.userImage sd_setImageWithURL:[user objectForKey:@"profilePicture"]];
    }
    else {
        
        PFObject *contact = member;
        
        [cell.userNameLabel setText:[contact objectForKey:@"completeName"]];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

#pragma mark - Action

- (IBAction)closeButtonPressed:(id)sender {
    
    //TODO: Unregister observer from CSNotificationView before dismissing or it will crash
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonPressed:(id)sender {
    
    NSString *errorMessage = [self surveyHasInvalidData];
    
    [self.saveButtonItem setEnabled:NO];
    
    if (!errorMessage) {
        
        CSNotificationView *notificationWithActivity = [CSNotificationView notificationViewWithParentViewController:self tintColor:[DFCColors blueColor] image:nil message:NSLocalizedString(@"CreatingGroup", nil)];
        notificationWithActivity.showingActivity = YES;
        [notificationWithActivity setVisible:YES animated:YES completion:nil];

        [DFCParseGroupHandler createGroupWithName:self.groupNameTextField.text image:self.groupPhoto andMembers:self.members withCompletition:^(BOOL succeeded){
        
            if (succeeded) {
                
                [notificationWithActivity dismissWithStyle:CSNotificationViewStyleSuccess message:NSLocalizedString(@"GroupCreatedCorrectly", nil)
                                                  duration:kCSNotificationViewDefaultShowDuration animated:YES withCompletition:^{
                                                  
                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                  }];
            }
            else {
                
                NSString *errorMessage = NSLocalizedString(@"UnknownError", nil);
                
                [CSNotificationView showInViewController:self
                                                   style:CSNotificationViewStyleError
                                                 message:errorMessage
                                        withCompletition:^{}];
            }
            
            [self.saveButtonItem setEnabled:YES];
        }];
    }
    else {
        
        [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleError
                                         message:errorMessage
                                withCompletition:^{}];
        
        [self.saveButtonItem setEnabled:YES];
    }
}

- (IBAction)backgroundPressed:(id)sender {
    
    [self.groupNameTextField resignFirstResponder];
}

- (IBAction)addMembersButtonPressed:(id)sender {
    
    [self performSegueWithIdentifier:@"addgroupToFriends" sender:nil];
}

- (IBAction)uploadImageButtonPressed:(id)sender {
    
    [self.imagePickerHandler showOptions];
}

#pragma mark - DFCUsersViewControllerDelegate

- (void)selectedUsers:(NSArray *)users {
    
    //TODO: Remove guest users if group isn't created
    NSArray *addedUsers = [DFCParseUserHandler saveUsers:users];
    
    [self.members addObjectsFromArray:addedUsers];
    
    [PFObject saveAllInBackground:self.members block:^(BOOL succeeded, NSError *error){
    
        [self.membersTableView reloadData];
    }];
}

#pragma mark - DFCImagePickerHandlerDelegate

- (void)imageSelected:(UIImage *)imageSelected {
    
    NSData *imageData = UIImageJPEGRepresentation(imageSelected, 0.05f);
    
    [self.uploadImageButton setBackgroundImage:imageSelected forState:UIControlStateNormal];
    
    [self uploadImage:imageData];
}

@end
