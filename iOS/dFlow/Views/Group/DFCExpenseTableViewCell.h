//
//  DFCExpenseTableViewCell.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 22/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface DFCExpenseTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *expenseImage;
@property (weak, nonatomic) IBOutlet UILabel *expenseSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseAuthorLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseCreationLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseAmountLabel;

- (void)fillCellWithExpense:(PFObject *)expense;

@end
