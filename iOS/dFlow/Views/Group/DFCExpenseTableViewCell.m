//
//  DFCExpenseTableViewCell.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 22/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCExpenseTableViewCell.h"

#import "DFCImagesUtils.h"

@implementation DFCExpenseTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
    
        // Initialization code
    }
    
    return self;
}

- (void)awakeFromNib {
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - public

- (void)fillCellWithExpense:(PFObject *)expense {
    
    [self.expenseSubjectLabel setText:[expense objectForKey:@"subject"]];
    [self.expenseAmountLabel setText:[NSString stringWithFormat:@"%.02f €", [[expense objectForKey:@"amount"] floatValue]]];
    
    //Author
    PFQuery *authorQuery = [PFUser query];
    
    NSString *authorId = [[expense objectForKey:@"payer"] objectId];
    
    [authorQuery whereKey:@"objectId" equalTo:authorId];
    
    [authorQuery getObjectInBackgroundWithId:authorId block:^(PFObject *object, NSError *error){
        
        PFUser *author = (PFUser *)object;
        [self.expenseAuthorLabel setText:[author objectForKey:@"name"]];
        
        [self.expenseImage sd_setImageWithURL:[author objectForKey:@"profilePicture"]];
    }];
    
    [DFCImagesUtils roundedImage:self.expenseImage];
    
    //Date
    NSDate *date = [expense objectForKey:@"date"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"dd/MM/yy"];
    
    [self.expenseCreationLabel setText:[dateFormat stringFromDate:date]];
}

@end
