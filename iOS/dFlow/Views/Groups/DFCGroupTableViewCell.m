//
//  DFCGroupTableViewCell.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCGroupTableViewCell.h"

#import "DFCColors.h"

#import "DFCImagesUtils.h"

@implementation DFCGroupTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
    
        // Initialization code
    }
    
    return self;
}

- (void)awakeFromNib {
    
    // Initialization code
    [self.groupDate setTextColor:[DFCColors mediumGrayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - public

- (void)fillGroupCellWithGroup:(PFObject *)group {
    
    float debtAmount = 0.0;
    
    //Name
    NSString *groupName = [group objectForKey:@"name"];
    
    [self.groupNameLabel setText:groupName];
    
    //Image
    PFObject *groupImage = [group objectForKey:@"image"];
    
    PFQuery *imageQuery = [PFQuery queryWithClassName:@"GroupPhoto"];
    
    if (groupImage.objectId) {
        
        [imageQuery whereKey:@"objectId" equalTo:groupImage.objectId];
        
        [imageQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
            
            PFFile *imageFile = [[objects firstObject] objectForKey:@"imageFile"];
            
            [self.groupImage sd_setImageWithURL:[NSURL URLWithString:[imageFile url]]];
        }];
    }
    
    [DFCImagesUtils roundedImage:self.groupImage];
    
    //Date
    NSDate *updated = [group updatedAt];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"dd/MM/yy"];
    
    [self.groupDate setText:[dateFormat stringFromDate:updated]];
    
    //Debt
    NSArray *usersDebts = [group objectForKey:@"usersDebts"];
    
    for (NSDictionary *userDebt in usersDebts) {
        
        PFUser *user = [userDebt objectForKey:@"user"];
        
        if ([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            
            debtAmount = [[userDebt objectForKey:@"debt"] floatValue];
            
            [self.groupAmount setText:[NSString stringWithFormat:@"%.2f €", debtAmount]];
        }
    }
    
    if (debtAmount >= 0.0) {
        
        [self.groupAmount setTextColor:[DFCColors blueColor]];
    }
    else {
        
        [self.groupAmount setTextColor:[DFCColors redColor]];
    }
    
    if (!usersDebts) {
        
        [self.groupAmount setText:@"0.00 €"];
    }
}

@end
