//
//  DFCGroupTableViewCell.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface DFCGroupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *groupImage;
@property (weak, nonatomic) IBOutlet UILabel *groupDate;
@property (weak, nonatomic) IBOutlet UILabel *groupAmount;

- (void)fillGroupCellWithGroup:(PFObject *)group;

@end
