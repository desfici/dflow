//
//  DFCUserTableViewCell.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCUserTableViewCell.h"

@implementation DFCUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
