//
//  DFCExpenseUsersViewController.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 26/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCExpenseUsersViewController.h"

#import "DFCUserTableViewCell.h"

#import "DFCContactsUtils.h"

@interface DFCExpenseUsersViewController ()

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSMutableArray *guestUsers;
@property (strong, nonatomic) NSMutableArray *selectedUsers;

- (void)selectSection:(int)section andRow:(int)row;
- (void)deselectSection:(int)section andRow:(int)row;

@end

@implementation DFCExpenseUsersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.closeButtonItem setTitle:NSLocalizedString(@"Close", nil)];
    [self.saveButtonItem setTitle:NSLocalizedString(@"Save", nil)];
    
    [self.selectAllButton setTitle:NSLocalizedString(@"SelectAll", nil) forState:UIControlStateNormal];
    
    self.selectedUsers = [[NSMutableArray alloc] init];
    
    [[[self.group relationForKey:@"users"] query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
    
        if (!error) {
            
            if (!self.users) {
                
                self.users = [[NSMutableArray alloc] init];
            }
            
            [self.users addObjectsFromArray:objects];
            
            [[[self.group relationForKey:@"guestUsers"] query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
                
                if (!error) {
                    
                    if (!self.guestUsers) {
                        
                        self.guestUsers = [[NSMutableArray alloc] init];
                    }
                    
                    [self.guestUsers addObjectsFromArray:objects];
                    
                    [self.usersTableView reloadData];
                }
                else {
                    
                    NSLog(@"Error getting guest users in expense users section");
                }
            }];
        }
        else {
            
            NSLog(@"Error getting users in expense users section");
        }
    }];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)selectSection:(int)section andRow:(int)row {
    
    switch (section) {
            
        case 0:
            
            [self.selectedUsers removeObject:[self.users objectAtIndex:row]];
            [self.selectedUsers addObject:[self.users objectAtIndex:row]];
            
            break;
            
        case 1:
            
            [self.selectedUsers removeObject:[self.guestUsers objectAtIndex:row]];
            [self.selectedUsers addObject:[self.guestUsers objectAtIndex:row]];
            
            break;
    }
    
    if ([self.selectedUsers count] == [self.users count] + [self.guestUsers count]) {
        
        [self.selectAllButton setTitle:NSLocalizedString(@"DeselectAll", nil) forState:UIControlStateNormal];
    }
}

- (void)deselectSection:(int)section andRow:(int)row {
    
    switch (section) {
            
        case 0:
            
            [self.selectedUsers removeObject:[self.users objectAtIndex:row]];
            
            break;
            
        case 1:
            
            [self.selectedUsers removeObject:[self.guestUsers objectAtIndex:row]];
            
            break;
    }
    
    if ([self.selectedUsers count] == 0) {
        
        [self.selectAllButton setTitle:NSLocalizedString(@"SelectAll", nil) forState:UIControlStateNormal];
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    int numberOfRows = 0;
    
    switch (section) {
            
        case 0:
            
            numberOfRows = [self.users count];
            
            break;
            
        case 1:
            
            numberOfRows = [self.guestUsers count];
            
            break;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DFCUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ParticipantCell" forIndexPath:indexPath];
    
    PFUser *user;
    PFObject *guestUser;
    
    switch (indexPath.section) {
            
        case 0:
            
            user = [self.users objectAtIndex:indexPath.row];
            
            [cell.userNameLabel setText:[user objectForKey:@"name"]];
            [cell.userImage sd_setImageWithURL:[user objectForKey:@"profilePicture"]];
            
            break;
            
        case 1:
            
            guestUser = [self.guestUsers objectAtIndex:indexPath.row];
            
            [cell.userNameLabel setText:[guestUser objectForKey:@"completeName"]];
            
            break;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self selectSection:indexPath.section andRow:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self deselectSection:indexPath.section andRow:indexPath.row];
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonPressed:(id)sender {
    
    [self.delegate selectedUsers:self.selectedUsers];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectAllButtonPressed:(id)sender {
    
    if ([[self.selectAllButton titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"SelectAll", nil)]) {
        
        for (int i = 0; i < [self.usersTableView numberOfSections]; i++) {
            
            for (int j = 0; j < [self.usersTableView  numberOfRowsInSection:i]; j++) {

                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                
                [self.usersTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                
                [self selectSection:indexPath.section andRow:indexPath.row];
            }
        }
    }
    else {
        
        for (int i = 0; i < [self.usersTableView numberOfSections]; i++) {
            
            for (int j = 0; j < [self.usersTableView  numberOfRowsInSection:i]; j++) {
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                
                [self.usersTableView deselectRowAtIndexPath:indexPath animated:YES];
                
                [self deselectSection:indexPath.section andRow:indexPath.row];
            }
        }
    }
}

@end
