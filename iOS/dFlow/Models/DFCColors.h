//
//  DFCColors.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFCColors : NSObject

+ (UIColor *)blueColor;
+ (UIColor *)lightGrayColor;
+ (UIColor *)mediumGrayColor;
+ (UIColor *)redColor;

@end
