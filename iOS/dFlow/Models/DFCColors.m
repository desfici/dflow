//
//  DFCColors.m
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 17/09/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import "DFCColors.h"

@implementation DFCColors

+ (UIColor *)blueColor {
    
    return [UIColor colorWithRed:21.0/255.0 green:158.0/255.0 blue:234.0/255.0 alpha:1.0];
}

+ (UIColor *)lightGrayColor {
    
    return [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
}

+ (UIColor *)mediumGrayColor {
    
    return [UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0];
}

+ (UIColor *)redColor {
    
    return [UIColor colorWithRed:213.0/255.0 green:20.0/255.0 blue:28.0/255.0 alpha:1.0];
}

@end
