//
//  DFCUsersViewController.h
//  dFlow
//
//  Created by Daniel Devesa Derksen-Staats on 19/08/14.
//  Copyright (c) 2014 Desfici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol DFCUsersViewControllerDelegate

- (void)selectedUsers:(NSArray *)users;

@end

@interface DFCUsersViewController : UIViewController

@property id delegate;

@property (weak, nonatomic) IBOutlet UITableView *usersTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;

@end
